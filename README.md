# [RIPSters](https://gitlab.com/zeen3/ripsters/)

Various site ripping scripts; tries to be high consistency.

Requires greasemonkey or tampermonkey.

Scripts are tested in few situations; and generally only on chromium:latest using tampermonkey.

Some scripts are more necessary than others (i.e.: can easily grab files off the site) but perform useful functions, such as slicing to a notable break in the image.

Please contribute your own.

## Usage restrictions

There are no primary usage restrictions; as there are no ways of resolving who's used it.

Thank you.

## Preface

Currently, these files are left unsorted. For example, `drawimage-catcher` is primarily for debugging (aka, unneeded in the majority case) and ripster-shared is just a general helper. Anything marked explicitly as lq has known higher quality options, and access can be requested through [aeiou#applications][]. See [Housekeeping](#housekeeping) for details.
