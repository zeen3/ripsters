## Notes

- I only test with new(er) versions of chromium; currently on 71 with [TamperMonkey](https://chrome.google.com/webstore/detail/dhdgffkkebhmkfjojejmpbldmpobfkfo). Whilst I can support firefox no specific efforts are made to do so.
- Ensure you disable native notifications (`chrome://flags/#enable-native-notifications` disabled)
- May not be needed, but web platform features enabled (`chrome://flags/#enable-experimental-web-platform-features` enabled)
- Same but with experimental JS (`chrome://flags/#enable-javascript-harmony` enabled)
- On the page, when ripping, you will need to enable multiple downloads through the site settings UI (omnibox &rarr; view site information &rarr; site settings); or by enabling multiple downloads by pressing the enable multiple downloads button that comes up after the first file (usually some placement.json or placement.xml variant) in the omnibox.

After setting those three, reload chrome/chromium using the button that pops up at the bottom.

## Housekeeping

Along with various other private, in-development, otherwise unreleased rippers, and some bots that other people have written; other tools are available given access in discord [aeiou#applications][] and [aeiou#bot-spam][]. Please request and use these, as opposed to keeping silent in these regards or directly asking me; as I may be incapacitated. I am only the upkeeper of this site and its tools, and other applications and requests should be made in their respective categories. If you ask me directly, I'm going to be silent since you haven't bothered to read this.

[aeiou#applications]: https://discord.gg/gqYDyuN
[aeiou#bot-spam]: https://discord.gg/mTJYYx3
[aeiou]: https://discord.gg/YwgVQfH
